<?php

use Pulsar\Client;
use Pulsar\MessageBuilder;
use Pulsar\ProducerConfiguration;
use Pulsar\Result;
use Pulsar\SchemaType;

$client = new Client();
$clientConfig = new ClientConfiguration();
$clientConfig->setLogLevel(E_ERROR);
$client->init('pulsar://127.0.0.1:6650', $clientConfig);

$schema = json_encode([
    'type' => 'record', // ??
    'name' => 'test',
    'fields' => [
        [
            'name' => 'foo',
            'type' => 'string',
        ],
        [
            'name' => 'iteration',
            'type' => 'int',
        ],
    ],
]);

$config = new ProducerConfiguration();
$config->setSchema(SchemaType::AVRO, 'test', $schema, []);

$producer = $client->createProducer('test-topic-with-schema', $config);

$i = 0;
while (true) {
    $prop = [
        'a' => ++$i,
    ];

    $builder = new MessageBuilder();
    $builder->setContent(json_encode([
        'foo' => 'bar',
        'iteration' => $i,
    ]))
    ->setProperties($prop)
    ->setEventTimestamp(time());

    $message = $builder->setDeliverAfter(300)->build();
    $result = $producer->send($message);

    if ($result === Result::ResultOk) {
        echo "\n success";
    } else {
        echo "\n could not send $result";
    }

    sleep(10);
}
