#include "ClientConfiguration.h"
#include <pulsar/ConsoleLoggerFactory.h>

Php::Value ClientConfiguration::setLogLevel(Php::Parameters &params) {
   
   pulsar::Logger::Level level;

   switch (params[0].numericValue()) {
      case 1:
         level = pulsar::Logger::LEVEL_ERROR;
         break;
      case 2:
         level = pulsar::Logger::LEVEL_WARN;
         break;
      case 32767:
         level = pulsar::Logger::LEVEL_DEBUG;
         break;
      default:
         level = pulsar::Logger::LEVEL_INFO;
   } 

   this->config.setLogger(new ::pulsar::ConsoleLoggerFactory(level));
   return this;
}

Php::Value ClientConfiguration::setUseTls(Php::Parameters &params) {
   this->config.setUseTls(params[0]);
   return this;
}

Php::Value ClientConfiguration::setTlsTrustCertsFilePath(Php::Parameters &params) {
   this->config.setTlsTrustCertsFilePath(params[0]);
   return this;
}

Php::Value ClientConfiguration::setTlsAllowInsecureConnection(Php::Parameters &params) {
   this->config.setTlsAllowInsecureConnection(params[0]);
   return this;
}

void registerClientConfiguration(Php::Namespace &pulsarNamespace) {
    Php::Class<ClientConfiguration> clientConfiguration("ClientConfiguration");
    clientConfiguration.method<&ClientConfiguration::setLogLevel>("setLogLevel");
    clientConfiguration.method<&ClientConfiguration::setUseTls>("setUseTls");
    clientConfiguration.method<&ClientConfiguration::setTlsTrustCertsFilePath>("setTlsTrustCertsFilePath");
    clientConfiguration.method<&ClientConfiguration::setTlsAllowInsecureConnection>("setTlsAllowInsecureConnection");
    pulsarNamespace.add(clientConfiguration);
}
